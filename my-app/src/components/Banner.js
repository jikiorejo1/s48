import React from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner() {
	

	return(

	<Row>
		<Col className="p-5">
			<h1 className="mb-3">Batch 165 Zuitt Coding Bootcamp</h1>
			<p className="my-3">Opportunities for everyone, everywhere</p>
			<Link to="courses"><Button variant="primary"> Enroll now!</Button></Link>
		</Col>
	</Row>
		)
}